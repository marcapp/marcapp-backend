#!/bin/bash
# Genera los scripts SQL para crear las tablas en las bases de datos
set -e

# Nombre del archivo con las queries para crear tablas
filename=createdb.sql

# Microservicios y sus respectivos archivos SQL
declare -A microservices
microservices[user-microservice]="db/sql/initdb.sql"
microservices[registration-microservice]="db/sql/initdb.sql"

# Crea el script y lo compara con el anterior, si lo hay
tempfile="$(mktemp)"
for ms in "${!microservices[@]}"; do
  cat >> "$tempfile" <<EOF
CREATE DATABASE "$ms";
\c $ms
CREATE EXTENSION postgis;
EOF
  cat "${ms}/${microservices[$ms]}" >> "$tempfile"
done

if test -e "$filename"; then
  diff -u "$filename" "$tempfile" || true
fi
mv "$tempfile" "$filename"
chmod go+r "$filename"

echo "$filename generado con éxito"
