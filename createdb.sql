CREATE DATABASE "user-microservice";
\c user-microservice
CREATE EXTENSION postgis;
-- Crear el tipo ENUM "users_role_enum"
CREATE TYPE users_role_enum AS ENUM ('User', 'Admin');

-- Crear la tabla "users"
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    "lastName" VARCHAR NOT NULL,
    address VARCHAR NOT NULL,
    phone_number VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    profile_picture VARCHAR NOT NULL DEFAULT '',
    role users_role_enum NOT NULL DEFAULT 'User',
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    is_active BOOLEAN NOT NULL DEFAULT false,
    is_deleted BOOLEAN NOT NULL DEFAULT false
);
CREATE DATABASE "registration-microservice";
\c registration-microservice
CREATE EXTENSION postgis;
CREATE TABLE "register" (
    "id" SERIAL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "entry" TIMESTAMP WITH TIME ZONE NOT NULL,
    "exit" TIMESTAMP WITH TIME ZONE,
    "worked_hours" numeric,
    "day" integer,
    "author_role" character varying NOT NULL,
    "entry_location" geometry(Point,4326),
    "exit_location" geometry(Point,4326)
);

CREATE TABLE "monthly_work_average" (
    "id" SERIAL PRIMARY KEY,
    "year" integer NOT NULL,
    "user_id" integer NOT NULL,
    "monthly_averages" json NOT NULL,
    CONSTRAINT "UQ_a1176b84f29114a0d61ef58c970" UNIQUE ("year", "user_id")
);
